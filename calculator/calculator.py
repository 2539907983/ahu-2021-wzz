class Count(object):                                    # 定义一个Count类

    def __init__(self, suanshi):                        # 使用__init__方法进行变量初始化
        self.suanshi = suanshi
        fuhao = self.fenxi()[0][0]                      # 调用实例方法fenxi
        a = int(self.fenxi()[1][0])                     # 将数字由str转化为int，便于计算
        b = int(self.fenxi()[1][2])
        if fuhao == '+':                                # 进行判断并且调用不同实例方法
            self.add(a, b)
        elif fuhao == '-':
            self.sub(a, b)
        elif fuhao == '*':
            self.multiply(a, b)
        else:
            self.division(a, b)

    def fenxi(self):
        from re import findall                          # 使用正则获得运算符和数字
        return findall('\W', self.suanshi), findall('\d*', self.suanshi)
                                                        
    def add(self, a, b):                                #加法方法
        print('结果为:%0.2f' % (a + b))

    def sub(self, a, b):                                #减法方法
        print('结果为:%0.2f' % (a - b))

    def multiply(self, a, b):                           #乘法方法
        print('结果为:%0.2f' % (a * b))

    def division(self, a, b):                           #除法方法
        print('结果为:%0.2f' % (a / b))


if __name__ == '__main__':                              # 运行
    Count(input('请输入计算式，回车以推退出：'))


