```python
import pymysql                                                                          # 导入包
connect = pymysql.connect(host="127.0.0.1",port=3306,user="root",password="")           # 连接数据库
cursor = connect.cursor()                                                               # 创建游标

cursor.execute('create database AHU_2021_TEST')                                         # 创建数据库AHU_2021_TEST
cursor.fetchall()

cursor.execute('use AHU_2021_TEST')                                                     # 路径切换到目标数据库中
cursor.fetchall()   

cursor.execute('create table calc_record('                                              # 创建数据表
               'id int not null auto_increment primary key,'                            # 设置主键为int型，非空，自增，不能重复且不为空
               'operand1 int,'                                                          # 设置第一个运算数为int型
               'operaor char,'                                                          # 设置运算符为char型
               'operand2 int,'                                                          # 设置第二个运算数为int型
               'result float)')                                                         # 设置结果为float型
cursor.fetchall()

cursor.close()
connect.close()
```