from flask import Flask, request                                          # 导入相关包，模块
import pymysql

count = Flask(__name__)                                                   # 实例化Flask


@count.route('/add')                                                      # 使用装饰器包裹函数
def add():                                                                # 定义函数
    a = int(request.args.get('a'))                                        # 获取变量a，b
    b = int(request.args.get('b'))
    result = a + b                                                        # 计算结果并保存在result中

    mysql(a, '+', b, result)                                              # 调用定义的函数mysql

    return '结果为:%0.2f' % result                                         # 向网页返回结果


@count.route('/sub')
def sub():
    a = int(request.args.get('a'))
    b = int(request.args.get('b'))
    result = a - b

    mysql(a, '-', b, result)

    return '结果为:%0.2f' % result


@count.route('/multiply')
def multiply():
    a = int(request.args.get('a'))
    b = int(request.args.get('b'))
    result = a * b

    mysql(a, '*', b, result)

    return '结果为:%0.2f' % result


@count.route('/division')
def division():
    a = int(request.args.get('a'))
    b = int(request.args.get('b'))
    result = a / b

    mysql(a, '/', b, result)

    return '结果为:%0.2f' % result


def mysql(operand1, operaor, operand2, result):                                                                 # 定义函数mysql，并设置四个参数
    ahu_2021_test = pymysql.connect(host="127.0.0.1", port=3306, user="root", password="", db="AHU_2021_TEST")  # 创建连接
    cursor = ahu_2021_test.cursor()                                                                             # 创建游标

    cursor.execute("insert into calc_record (operand1,operaor,operand2,result) "                                # 使用SQL语句插入数据
                   "values (%d,'%s',%d,%0.2f)" % (operand1, operaor, operand2, result))
    cursor.fetchall()                                                                                           # 执行SQL语句
    ahu_2021_test.commit()                                                                                      # 提交数据，使数据得到保存

    cursor.close()                                                                                              # 关闭游标
    ahu_2021_test.close()                                                                                       # 关闭数据库


if __name__ == '__main__':
    count.run()                                                                                                 # 调用run方法执行代码
