from flask import Flask, request                                     # 导入Flask，request

count = Flask(__name__)                                              # 实例化Flask


@count.route('/add')                                                 # 使用装饰器修饰
def add():                                                           # 定义函数
    a = int(request.args.get('a'))                                   # 获取参数
    b = int(request.args.get('b'))
    result = a + b                                                   # 计算结果

    return '结果为:%0.2f' % result                                    # 返回值


@count.route('/sub')
def sub():
    a = int(request.args.get('a'))
    b = int(request.args.get('b'))
    result = a - b

    return '结果为:%0.2f' % result


@count.route('/multiply')
def multiply():
    a = int(request.args.get('a'))
    b = int(request.args.get('b'))
    result = a * b

    return '结果为:%0.2f' % result


@count.route('/division')
def division():
    a = int(request.args.get('a'))
    b = int(request.args.get('b'))
    result = a / b

    return '结果为:%0.2f' % result


if __name__ == '__main__':                                           # 开始执行
    count.run()                                                      # 调用count对象的run方法
